const http = require('http')
const qs = require('querystring')

const app = () => {
  if (process.argv.length < 3) {
    console.error(`Usage: ${__filename} port`)
    return process.exit(1)
  }

  const port = process.argv[2]

  const server = http.createServer((req, res) => {
    const request_path = requestPath(req)

    switch (request_path) {
      case '/api/parsetime':
      case '/api/unixtime':
      {
        const query = requestQuery(req)

        if ('iso' in query) {
          const date = tryMakeDateOfIsoString(query.iso)

          if (date !== null) {
            const hour = date.getHours()
            const minute = date.getMinutes()
            const second = date.getSeconds()

            const payload = request_path === '/api/unixtime'
              ? { unixtime: date.getTime() }
              : { hour, minute, second }

            res.writeHead(200, { 'content-type': 'application/json' })
            res.end(JSON.stringify(payload))

            return
          }
        }

        res.writeHead(400)
        res.end()

        return
      }

      default: {
        res.writeHead(404)
        res.end()

        return
      }
    }
  })

  return server.listen(port)
}

const requestQuery = req => {
  const parts = req.url.split('?')

  switch (parts.length) {
    case 1:
      return {}
      
    case 2:
      return { ...qs.parse(parts[1]) }

    default:
      throw new Error('Malformed url')
  }
}

const requestPath = req => req.url.split('?')[0]

const tryMakeDateOfIsoString = str => {
  if (typeof str !== 'string') {
    throw new Error('"str" must be a string')
  }

  const date = new Date(str)
  const is_valid = !Number.isNaN(date.getTime())

  if (is_valid) {
    const is_from_iso = date.toISOString() === str

    if (is_from_iso) {
      return date
    }
  }

  return null
}

app()
