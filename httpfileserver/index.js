const http = require('http')
const fs = require('fs')

const app = () => {
  if (process.argv.length < 4) {
    console.error(`Usage: ${__filename} port path-to-view`)
    return process.exit(1)
  }

  const port = process.argv[2]
  const view = process.argv[3]

  const server = http.createServer((req, res) => {
    res.writeHead(200, { 'content-type': 'text/plain' })
    return fs.createReadStream(view).pipe(res)
  })

  return server.listen(port)
}

app()
