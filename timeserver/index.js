const net = require('net')

const app = () => {
  if (process.argv.length < 3) {
    console.error(`Usage: ${__filename} port`)
    return process.exit(1)
  }

  const port = process.argv[2]

  const server = net.createServer(sock => {
    const now = new Date()
    return sock.end(formatDate(now) + '\n')
  })

  return server.listen(port)
}

const formatDate = date => {
  const year = date.getFullYear().toString().padStart(4, '0')
  const month = (1 + date.getMonth()).toString().padStart(2, '0')
  const day = date.getDate().toString().padStart(2, '0')
  const hrs = date.getHours().toString().padStart(2, '0')
  const mins = date.getMinutes().toString().padStart(2, '0')

  return `${year}-${month}-${day} ${hrs}:${mins}`
}

app()
