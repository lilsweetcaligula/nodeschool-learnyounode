const http = require('http')
const stream = require('stream')

const app = () => {
  if (process.argv.length < 3) {
    console.error(`Usage: ${__filename} port`)
    return process.exit(1)
  }

  const port = process.argv[2]

  const server = http.createServer((req, res) => {
    switch (req.method) {
      case 'POST': {
        res.writeHead(200, { 'content-type': 'text/plain' })
        return req.pipe(uppercaser()).pipe(res)
      }

      default: {
        res.writeHead(404)
        return res.end()
      }
    }
  })

  return server.listen(port)
}

const uppercaser = () => new stream.Transform({
  transform(chunk, enc, cb) {
    return cb(null, chunk.toString('utf-8').toUpperCase())
  }
})

app()
