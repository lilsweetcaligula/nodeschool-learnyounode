const fs = require('fs')
const util = require('util')
const path = require('path')
const promiseReadDir = util.promisify(fs.readdir.bind(fs))

const app = async () => {
  if (process.argv.length < 4) {
    throw new Error(`Usage: ${__filename} path ext`)
  }

  const dirpath = process.argv[2]
  const target_ext = process.argv[3]
  const hasTheExtension = filename => path.extname(filename) === `.${target_ext}`

  const dir_entries = await promiseReadDir(dirpath, { withFileTypes: true })
  const files = dir_entries.filter(entry => entry.isFile())
  const target_files = files.filter(file => hasTheExtension(file.name))

  const result = target_files.map(file => file.name).join('\n')

  console.log(result)
}

app().catch(err => {
  console.error(err.message)
  return process.exit(1)
})

