const http = require('http')

const app = () => {
  if (process.argv.length < 3) {
    console.error(`Usage: ${__filename} url`)
    return process.exit(1)
  }

  const url = process.argv[2]

  const req = http.get(url, res => {
    res.on('data', data => {
      console.log(data.toString('utf-8'))
    })

    res.on('error', handleAppError)
  })

  req.on('error', handleAppError)

  req.end()
}

const handleAppError = err => {
  console.error('Error:', err.message)
  return process.exit(1)
}

app()
