const fs = require('fs')
const util = require('util')
const promiseReadFile = util.promisify(fs.readFile.bind(fs))

const app = async () => {
  if (process.argv.length < 3) {
    throw new Error(`Usage: ${__filename} path-to-file`)
  }

  const filepath = process.argv[2]

  const content = await promiseReadFile(filepath, { encoding: 'utf-8' })
  const num_lines = content.replace(/[^\n]/g, '').length

  console.log(num_lines)
}

app().catch(err => {
  console.error(err.message)
  return process.exit(1)
})
