const http = require('http')

const app = async () => {
  if (process.argv.length < 3) {
    console.error(`Usage: ${__filename} url`)
    return process.exit(1)
  }

  const url = process.argv[2]
  const response = await httpGet(url)

  console.log(response.body.length)
  console.log(response.body.toString('utf-8'))
}

const httpGet = (...args) => new Promise((resolve, reject) => {
  const req = http.get(...args, res => {
    let buf

    buf = Buffer.from([])

    res.on('data', data => {
      buf = Buffer.concat([buf, data])
    })

    res.on('end', () => {
      const { statusCode } = res
      resolve({ statusCode, body: buf })
    })

    res.on('error', reject)
  })

  req.on('error', reject)
})

const handleAppError = err => {
  console.error('Error:', err.message)
  return process.exit(1)
}

app().catch(handleAppError)

