const http = require('http')

const app = async () => {
  if (process.argv.length < 5) {
    throw new Error(`Usage: ${__filename} url1 url2 url3`)
  }

  const urls = process.argv.slice(2, 5)
  const responses = await Promise.all(urls.map(httpGet))

  console.log(responses.map(res => res.body).join('\n'))
}

const httpGet = url => new Promise((resolve, reject) => {
  const req = http.get(url, res => {
    let buf

    buf = Buffer.from([])

    res.on('data', data => {
      buf = Buffer.concat([buf, data])
    })

    res.on('end', () => {
      const { statusCode } = res
      resolve({ statusCode, body: buf })
    })

    res.on('error', reject)
  })

  req.on('error', reject)
})

app().catch(err => {
  console.error(err.message)
  return process.exit(1)
})
