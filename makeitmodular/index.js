const lsFiltered = require('./mymodule.js')

const app = () => {
  if (process.argv.length < 4) {
    console.error(`Usage: ${__filename} path ext`)
    return process.exit(1)
  }

  const dirpath = process.argv[2]
  const sel_ext = process.argv[3]

  lsFiltered(dirpath, sel_ext, (err, files) => {
    if (err) throw err

    console.log(files.join('\n'))
  })
}

app()

process.on('uncaughtException', err => {
  console.error(err.message)
  return process.exit(1)
})

