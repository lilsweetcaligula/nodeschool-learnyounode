const fs = require('fs')
const path = require('path')

module.exports = function lsFiltered(dirpath, ext, cb) {
  fs.readdir(dirpath, { withFileTypes: true }, (err, dir_entries) => {
    if (err) {
      return cb(err)
    }

    const sel_files = dir_entries
      .filter(entry => entry.isFile())
      .filter(file => path.extname(file.name) === `.${ext}`)
      .map(file => file.name)

    return cb(null, sel_files)
  })
}
